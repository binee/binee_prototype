#!/bin/sh
## Repo Update
apt-get update

echo "Which binee are you setting up?"
echo "1 - presentation prototype"
echo "2 - cardboard prototoype"
read binee

### Create Config File
touch config.binee

### Create Folder Structure
mkdir /opt/binee
mkdir /var/log/binee
mkdir /var/binee

## Create necessary folders
mkdir /var/binee/pictures
mkdir /var/log/binee/supervisord

### Set Hostname and Config File
case $binee in
	"1")
	echo "Binee-Presentation" > /etc/hostname
	echo "127.0.1.1    Binee-Presentation" >> /etc/hosts
	echo "presentation" > config.binee
	;;
	"2")
	echo "Binee-Cardboard" > /etc/hostname
	echo "127.0.1.1    Binee-Cardboard" > /etc/hosts
	echo "cardboard" > config.binee
	;;
esac

/etc/init.d/hostname.sh start

ssh-keygen
echo "Start ssh-agent"
ssh-agent
echo "Add ssh ID"
ssh-add ~/.ssh/id_rsa
echo "This is the ssh key, Add it to your server before you proceed:"
cat ~/.ssh/id_rsa.pub
echo "If you are ready press any key"
read -n 1 -s

## GIT SETUP
case $binee in
	"1") git config --global user.name "Presentation_Binee"
	;;
	"2") git config --global user.name "Cardboard_Binee"
	;;
esac

git config --global user.email "florian@binee.com"

## CLONE BINEE REPOS
cd /opt/binee
git clone git@bitbucket.org:binee/binee_prototype.git
cd binee_prototype

## Get and install SPI tools
#apt-get install python-dev
#mkdir install
#git clone https://github.com/lthiery/SPI-Py.git install
#cd install
#python setup.py install

#Install pigpio for software pwm on any PIN (not interferring the sound)
#if [ "$binee" = "1" ]
#then
#    wget abyz.co.uk/rpi/pigpio/pigpio.zip
#    unzip pigpio.zip
#    cd PIGPIO
#    make
#    make install
#fi

### Install Python Package Manager
#wget https://bootstrap.pypa.io/get-pip.py
#python get-pip.py

### Install and Configure Supervisor
apt-get install supervisor
service supervisor restart

cp /opt/binee/binee_prototype/binee.conf /etc/supervisor/conf.d/

supervisorctl reread
supervisorctl update

### Install Required Packages
#pip install requests
#apt-get install python-imaging

# Make binee a service
#cd /home/pi/binee_prototype
#chmod 755 binee_main.py
#cp binee_service.sh /etc/init.d
#chmod 755 /etc/init.d/binee_service.sh
