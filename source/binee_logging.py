'''
Logging Module which is handling the Logging Config.
'''

# Imports
import logging
from logging.handlers import RotatingFileHandler
import os

# Module Statics
__author__ = 'David Moeller'
__version__ = "$Revision$"


class BineeLogger(object):
    '''
    To get the Logger simply assign it to a variable like
    self.logger = BineeLogger(BineeLogger.MANAGER, self.__class__.__name__)
    self.__class__.__name__ equals the String representation of a class
    '''

    # Categories
    (MANAGER, NFC, LED, NONE) = (0, 1, 2, 3)
    # New Levels
    (CRITICAL, ERROR, WARNING, TEST, INFO, DEBUG) = (50, 40, 30, 25, 20, 10)

    logger = {}

    added_level = False

    main_log_path = '/var/log/binee/'
    file_path = ''

    category = None

    def __init__(self, category, name):

        # TODO:All logs will be saved in one Manager File
        category = BineeLogger.MANAGER

        # Get File Path
        self.file_path = self.__get_path_for_class(category)

        # Add Test Level
        self.add_test_level()

        # Set Category
        self.category = category

        # Set Class Name
        self.name = name

    def debug(self, msg):
        self.__user_logger(self.DEBUG, msg)
        return msg

    def info(self, msg):
        self.__user_logger(self.INFO, msg)
        return msg

    def test(self, msg):
        self.__user_logger(self.TEST, msg)
        return msg

    def warning(self, msg):
        self.__user_logger(self.WARNING, msg)
        return msg

    def error(self, msg):
        self.__user_logger(self.ERROR, msg)
        return msg

    def critical(self, msg):
        self.__user_logger(self.CRITICAL, msg)
        return msg

    def __user_logger(self, level, msg):
        if self.category != self.NONE:
            logger = self.get_logger_for_name(self.name)
            logger.log(level, msg)

    @staticmethod
    def add_test_level():
        if BineeLogger.added_level is False:
            logging.addLevelName(BineeLogger.TEST, 'TEST')
            BineeLogger.added_level = True

    def get_logger_for_name(self, name):

        if name in self.logger:
            return self.logger[name]
        else:
            logger = self.__build_logger_for_name(name)
            self.logger[name] = logger
            return logger

    def __build_logger_for_name(self, name):

        level = logging.DEBUG

        logging.basicConfig(level=level,
                            format='%(levelname)s %(asctime)s %(name)s[%('
                                   'process)d] %(message)s')
        logger = logging.getLogger(name)

        # Get Rotating File Handler
        handler = RotatingFileHandler(self.file_path, maxBytes=1000000,
                                      backupCount=5)
        handler.setLevel(level=level)

        formatter = logging.Formatter(
            '%(asctime)s - %(levelname)s - %(name)s - %(message)s')
        handler.setFormatter(formatter)

        logger.addHandler(handler)

        return logger

    @staticmethod
    def __get_path_for_class(category):

        path = BineeLogger.main_log_path

        if not os.path.isdir(path):
            os.mkdir(path)

        if category == BineeLogger.MANAGER:
            path += 'manager.log'
        elif category == BineeLogger.NFC:
            path += 'nfc.log'
        elif category == BineeLogger.LED:
            path += 'led.log'
        else:
            path += 'leftOvers.log'

        return path


class StreamToLogger(object):

    """
   Fake file-like stream object that redirects writes to a logger instance.
   """

    def __init__(self, logger, log_level=logging.WARNING):
        self.logger = logger
        self.log_level = log_level
        self.linebuf = ''

    def write(self, buf):
        for line in buf.rstrip().splitlines():
            self.logger.log(self.log_level, line.rstrip())
