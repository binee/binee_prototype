'''
This module is containing the Speaker Specific Classes/Functions
'''

# Imports
from binee_device import Device
from binee_logging import BineeLogger

from source.utils.presentation.presentation_speaker import PresentationSpeaker

# Module Statics
__author__ = 'David Moeller'
__version__ = "$Revision$"


class Speaker:

    def __init__(self):
        # Init Logger
        self.logger = BineeLogger(BineeLogger.MANAGER, self.__class__.__name__)

    def say(self, text):

        if not Device.has_speaker():
            self.logger.error('Tried to Say something. But this Binee Model (' + str(Device.bin_model) +
                              ') has no Speaker.')
            return

        speaker = PresentationSpeaker()
        speaker.say(text)
