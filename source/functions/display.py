'''
This module is containing the Display Specific Classes/Functions
'''

# Imports
from binee_device import Device
from binee_logging import BineeLogger

from source.utils.cardboard import LcdDisplay

# Module Statics
__author__ = 'David Moeller'
__version__ = "$Revision$"


class Display:

    def __init__(self):
        # Init Logger
        self.logger = BineeLogger(BineeLogger.MANAGER, self.__class__.__name__)

    def display_text_upper_line(self, text):
        self.display_text_upper_and_lower_line(text)

    def display_text_lower_line(self, text):
        self.display_text_upper_and_lower_line(None, text)

    def display_text_upper_and_lower_line(self, upper=None, lower=None):
        # Check if Device is valid
        if not Device.has_display():
            self.logger.error('Tried to Display something. But this Binee Model (' + str(Device.bin_model) +
                              ') has no Display.')
            return

        display = LcdDisplay()

        # Write Properties
        properties = display.write_display_properties()

        # Write Upper Line
        upper_line = {}
        if upper is not None:
            upper_line = display.write_line_one(upper)

        # Write Lower Line
        lower_line = {}
        if lower is not None:
            lower_line = display.write_line_one(lower)

        # Dumb
        display.dump_display_file(properties, upper_line, lower_line)
