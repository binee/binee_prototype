'''
This module is containing the Speaker Specific Classes/Functions
'''

# Imports
from binee_device import Device
from binee_logging import BineeLogger
import picamera

# Module Statics
__author__ = 'David Moeller'
__version__ = "$Revision$"


class Camera:

    def __init__(self):
        # Init Logger
        self.logger = BineeLogger(BineeLogger.MANAGER, self.__class__.__name__)
        # Setup Camera
        self.cam = picamera.PiCamera()
        self.cam.resolution = (1000, 750)  # res.max= 2592x1944
        self.cam.brightness = 57

    def capture(self, filename):
        self.cam.capture(filename)
