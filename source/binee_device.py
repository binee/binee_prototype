'''
This Module is providing the Device Class.
'''

# Imports
import requests
import json

# Module Statics
__author__ = 'David Moeller'
__version__ = "$Revision$"


class Device:

    bin_model = None
    good_type = None
    bin_name = None

    bin_id = None

    display = False
    drum = False
    lid = False
    led = False
    speaker = False

    @classmethod
    def read_config(cls):
        # TODO: What happens if Reading Fails? Standard Parameters?
        # READ CONFIG FILE
        with open('config.binee') as f:
            conf = f.readlines()
            conf_lines = len(conf)
            for i in range(0,conf_lines):
                conf[i] = conf[i].replace('\n','')

            # TODO: Check if valid Model?
            cls.bin_model = conf[0]

        if cls.bin_model == 'cardboard':
            cls.display = True
            cls.lid = True
            cls.drum = True
            cls.led = True
        elif cls.bin_model == 'presentation':
            cls.lid = True
            cls.drum = True
            cls.led = True
            cls.speaker = True

        # Set Binee Parameters
        cls.good_type = 'cellphone'
        cls.bin_name = 'bernd'

    @classmethod
    def create_bin_id(cls):
        mac = cls.get_mac('wlan0')
        bin_id = cls.get_bin_id(mac)
        cls.bin_id = bin_id

    @classmethod
    def get_mac(cls, interface):
        # Get devices MAC-Adress, Interface= wlan0, eth0
        # Return MAC as String
        try:
            mac_string = open('/sys/class/net/%s/address' % interface).readline()
        except:
            mac_string = "00:00:00:00:00:00"
        return mac_string[0:17]

    @classmethod
    def get_bin_id(cls, mac_string):
        # Create BinID from MAC-Adress
        bin_id = mac_string[0:2] + mac_string[3:5] + mac_string[6:8] + mac_string[9:11] + mac_string[12:14] + \
                 mac_string[15:17]
        return bin_id

    @classmethod
    def get_location(cls):
        r = requests.get('http://freegeoip.net/json')
        j = json.loads(r.text)
        lat = j['latitude']
        lon = j['longitude']
        location = str(lat)+";"+str(lon)
        return location

    @classmethod
    def has_display(cls):
        return cls.display

    @classmethod
    def has_speaker(cls):
        return cls.speaker

    @classmethod
    def has_led(cls):
        return cls.led

    @classmethod
    def has_lid(cls):
        return cls.lid

    @classmethod
    def has_drum(cls):
        return cls.drum