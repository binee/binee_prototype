'''
Main File which is handling all states of the binee bin prototype.
'''

# Imports
import threading
import time
import os
import sys

from binee_device import Device
from binee_logging import BineeLogger

# High Level Function Classes
from functions.speaker import Speaker
from functions.cam import Camera
from functions.display import Display

# Threaded Classes
from threaded.led_control import LEDControl
from threaded.nfc_control import NFCControl
from threaded.drum_control import DrumControl
from threaded.lid_control import LidControl

# Low Level Class
from utils.server_communication import Communicator

# Module Statics
__author__ = 'David Moeller'
__version__ = "$Revision$"

# TODO: Maybe the Threading Subclass is a bad Idea in Combination with supervisor?
# TODO: Real State Machine?


class BineeManager():
    '''
    This Class is running within its own thread, managing the states of the bin.
    '''

    # This Dict is containing all possible states and their names
    bin_states = None
    # This Dict is containing all possible states and their specific state method
    bin_state_methods = None

    bin_state = None

    # Status of the Card (i.e. registered or not)
    card_status = None

    def __init__(self):
        '''
        Self explanatory.
        '''
        # Initialise the Logger
        self.logger = BineeLogger(BineeLogger.MANAGER, self.__class__.__name__)
        # Read the Config
        Device.read_config()
        # Setup
        self._setup()
        # Tell the World i'm ready!
        self.speaker.say("I'm ready")
        # Booting Sequence
        self._boot_up()

    def run(self):
        '''
        Run Method
        :return: None
        '''
        print 'Test'
        self.logger.info('Start Binee Manager Thread.')
        self._manage()

    def _setup(self):
        '''
        Setup all used object handler and the actuator controls
        '''

        # Initialize Objects
        # Setup Speaker
        self.speaker = Speaker()
        # Setup cam
        self.cam = Camera()
        # Setup Display
        self.display = Display()
        # Setup Server API
        self.communicator = Communicator.Instance()

        # Initialize Threads
        # Setup LEDs
        self.led_control = LEDControl()
        self.led_control.daemon = True
        self.led_control.start()
        # NFC Controls
        self.nfc_control = NFCControl()
        self.nfc_control.daemon = True
        self.nfc_control.start()
        # Setup Drum and Lid
        self.lid_control = LidControl()
        self.lid_control.daemon = True
        self.lid_control.start()
        self.drum_control = DrumControl()
        self.drum_control.daemon = True
        self.drum_control.start()

        # Setup states
        self.bin_states = {}
        self.bin_state_methods = {}
        '''
        scheme: [State Shortcut, State Name, State function(IMPORTANT: Without the parentheses)]
        '''
        init_bin_states = [
            ["locked", "null", self.placeholder],
            ["0", "booting", self.placeholder],
            ["1", "startPage", self.starting_page],
            ["1.1", "thankyou", self.placeholder],
            ["2.0", "preparePhase", self.prepare_phase],
            ["2.1", "helloRegisteredUser", self.placeholder],
            ["2.2", "helloForeigner", self.hello_foreigner],
            ["2.3", "firstContact", self.placeholder],
            ["3.0", "processing", self.processing],
            ["3.1", "takePhoto", self.take_photo],
            ["3.2", "thankYou", self.placeholder],
            ["3.3", "confirmKey", self.placeholder],
            ["3.4", "confirmPairing", self.placeholder],
            ["4.1", "userRegistered", self.placeholder],
            ["4.2", "error", self.placeholder],
            ["4.3", "missingResource", self.placeholder],
            ["5", "stopped", self.placeholder]
        ]
        self._init_states(init_bin_states)

    def _init_states(self, state_props):
        '''
        This method is taking a List with the Properties of the States.
        i.e. [['0','startingPage',self.starting_page]],
        scheme: [State Shortcut, State Name, State function (IMPORTANT: Without the parentheses)]
        '''

        for state in state_props:
            self.bin_states[state[0]] = state[1]
            self.bin_state_methods[state[0]] = state[2]

    def _boot_up(self):
        '''
        This Method is defining the Boot up Sequence.
        '''
        self._set_bin_state('0')
        # Starting Up
        self.logger.info('Starting Up.')
        self.display.display_text_upper_and_lower_line('binee starting', '...')
        self.led_control.set_mode(LEDControl.RUN)
        # Create Bin ID
        self.logger.info('Set Device Properties.')
        self.display.display_text_upper_and_lower_line('binee starting', 'inner values...')
        Device.create_bin_id()
        # Init Motor Positions
        self.logger.info('Set Drum and Lid to starting position.')
        self.display.display_text_upper_and_lower_line('binee starting', 'motors...')
        self.drum_control.set_command(DrumControl.INPUT)
        self.lid_control.set_command(LidControl.HARDCLOSE)
        # Check Server
        self.logger.info('Check Server Connection.')
        self.display.display_text_upper_and_lower_line('binee starting', 'server...')
        if not self.communicator.check_connection():
            self.logger.info('No Connection available. Switch to Offline Mode.')
        # Set State
        self._set_bin_state('1')

    def _set_bin_state(self, state):
        '''
        This method is checking the new set state.
        Furthermore it is sending the state to the Backend and is initiate the state method.
        '''

        if state not in self.bin_states:
            self.logger.error('State (' + state + ') is not valid.')
            return

        self.logger.info('Set new State (' + state + ').')
        # Set State
        self.bin_state = state
        # Set time
        self.last_state_change = time.time()
        # Send Status
        status_text = state + self.bin_states[state]
        self.communicator.post_request('/api/bins/states', data={'state': status_text})
        # Do Status Method
        method = self.bin_state_methods[state]
        method()

    def _manage(self):

        while True:
            # Check if Drum and Lid are still running
            if self.drum_control.is_running() or self.lid_control.is_running():
                continue
            # Check Card
            self._check_current_state()

    # ------------------------- State Checks ----------------------------#
    last_state_change = None

    def _check_time(self, delay):
        if time.time() > self.last_state_change + delay:
            return True
        return False

    def _check_current_state(self):
        '''
        Check the Conditions of the Current State to process to the next state, if conditions are met.
        '''

        # Check for State 1
        if self.bin_state == '1':
            if self.nfc_control.has_card():
                self._set_bin_state('2.0')
            return
        # Check for State 1.1
        elif self.bin_state == '1.1':
            if self._check_time(5):
                self._set_bin_state('1')
            return
        # Check for State 2.0
        elif self.bin_state == '2.0':
            if self.card_status == 1:
                self._set_bin_state('2.1')
            else:
                self._set_bin_state('2.2')
            return
        # Check for State 2.1
        elif self.bin_state == '2.1':
            if self.nfc_control.has_card() or self._check_time(10):
                self._set_bin_state('3.0')
            return
        # Check for State 2.2
        elif self.bin_state == '2.2':
            if self.nfc_control.has_card() or self._check_time(10):
                self._set_bin_state('3.0')
            return
        # Check for State 3.0
        elif self.bin_state == '3.0':
            self._set_bin_state('3.1')
            return
        # Check for State 3.1
        elif self.bin_state == '3.1':
            self._set_bin_state('1.1')
            return

    # ------------------------- State Methods ---------------------------- #

    def starting_page(self):
        self.logger.debug('Starting_Page')
        self.display.display_text_upper_and_lower_line('Hey, I am binee!', 'Identify via NFC.')
        self.led_control.set_mode(LEDControl.STATUS, color='green')

    def hello_foreigner(self):
        self.logger.debug('hello_foreigner')
        self.display.display_text_upper_and_lower_line('Hey gorgeous!', 'Feed me.')
        self.led_control.set_mode(LEDControl.STATUS, color='green')

    def prepare_phase(self):
        self.logger.debug('prepare_phase')
        # Open Lid
        self.lid_control.set_command(LidControl.OPEN)
        # Check if Card is paired
        self.card_status = self.communicator.post_request('/api/searches/pairings.json',
                                                     data={'token': self.nfc_control.card_hashed_id,
                                                           'type': self.nfc_control.card_type})

    def processing(self):
        self.logger.debug('processing')
        self.display.display_text_upper_and_lower_line('Thank you!', 'Your ID here.')
        self.lid_control.set_command(LidControl.CLOSE)
        self.drum_control.set_command(DrumControl.CAM)

    def take_photo(self):
        photo_folder = "/var/binee/pictures/"
        if not os.path.isdir(photo_folder):
            os.mkdir(photo_folder)
        # Photo Path for Cam
        photo_path = photo_folder + time.strftime('%Y-%m-%dT%H:%M:%S', time.localtime()) + "+01:00.jpg"
        self.photo(photo_path)
        self.drum_control.set_command(DrumControl.DUMP)

    def placeholder(self):
        self.logger.debug('placeholder')
        # Do Nothing
        text = 'Dummy'

    # ------------------------- Helper Methods ----------------------------#

    def photo(self, filename):
        self.logger.info('Taking Photo')
        self.led_control.set_cam_status('on')
        self.cam.capture(filename)
        self.led_control.set_cam_status("off")

if __name__ == '__main__':

    # Kill std out
    f = open(os.devnull, 'w')
    sys.stdout = f

    logger = BineeLogger(BineeLogger.MANAGER, __name__)

    logger.info('--------------------------------------')
    logger.info('Start Binee Process.')
    manager = BineeManager()
    manager.run()

