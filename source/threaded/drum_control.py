'''
This module is containing the Drum class
'''

# Imports
import threading
from time import sleep

from binee_device import Device
from binee_logging import BineeLogger
from utils.presentation.presentation_drum import PresentationDrum

from source.utils.cardboard import CardboardDrum

# Module Statics
__author__ = 'David Moeller'
__version__ = "$Revision$"


class DrumControl(threading.Thread):

    # Threading Propertie
    stop_flag = False

    # Interval for Event Loop
    interval = 0.5

    # Low Level Command Class
    drum_command_class = None

    # Event Loop Queue
    command_queue = None

    # Running
    running = None

    # Possible Commands
    (CAM, INPUT, OUTPUT, DUMP) = ('cam', 'input', 'output', 'dump')
    commands = ['cam', 'input', 'output', 'dump']

    def __init__(self):
        # Init of Superclass
        threading.Thread.__init__(self)
        # Set Logger
        self.logger = BineeLogger(BineeLogger.MANAGER, self.__class__.__name__)
        # Check if LEDs are available
        if not Device.has_drum():
            self.logger.warning('Initialized Drum-Control. But this '
                                'Binee Model (' + str(Device.bin_model) +
                                ') has no Drum.')

        if Device.bin_model == 'presentation':
            self.drum_command_class = PresentationDrum()
        elif Device.bin_model == 'cardboard':
            self.drum_command_class = CardboardDrum()

        self.command_queue = []
        self.running = False
        self.daemon = True

    def is_running(self):
        return self.running

    # ------------------- Threading Methods ----------------------#

    def run(self):
        self.logger.info('Start Drum Control Thread.')
        # Reset Stop Flag
        self.stop_flag = False
        # Start Loop
        self._manage()

    def stop(self):
        self.stop_flag = True

    # ------------------- Drum Commands ----------------------#

    def set_command(self, command):
        '''
        Set Command for Drum
        '''
        if not isinstance(command, str):
            raise TypeError(self.logger.error('Command is not of Type String.'))

        if command not in self.commands:
            raise ValueError(self.logger.error('Command is not available.'))

        self.command_queue.append(command)
        self.logger.info('Set Drum Command: ' + str(command))

    def _manage(self):

        while not self.stop_flag:
            # Interval between changes
            sleep(self.interval)
            # Check LED
            if self.drum_command_class is None:
                self.logger.warning('No Drum Command Class available.')
                self.stop_flag = True
                continue

            # Check Event Queue
            if len(self.command_queue) == 0:
                self.running = False
                continue

            self.running = True

            # Get first command and remove it from queue
            command = self.command_queue.pop(0)

            # Process command
            self.process_command(command)

    def process_command(self, command):
        try:
            if command == DrumControl.CAM:
                self.drum_command_class.cam_position()
            elif command == DrumControl.INPUT:
                self.drum_command_class.input_position()
            elif command == DrumControl.OUTPUT:
                self.drum_command_class.output_position()
            elif command == DrumControl.DUMP:
                self.drum_command_class.dump_position()
        except:
            self.drum_command_class.reset()
