'''
This module is containing the led Control class
'''

# Imports
import threading
from time import sleep

from binee_device import Device
from binee_logging import BineeLogger
from utils.presentation.presentation_led import PresentationLED

from source.utils.cardboard import CardboardLED

# Module Statics
__author__ = 'David Moeller'
__version__ = "$Revision$"


class LEDControl(threading.Thread):

    # TODO: What if binee main is killed and wants to get this thread back?

    # Mode
    # TODO: Needs better solution
    (RUN, BLINK, OFF, STATUS) = (0, 1, 2, 3)
    modes = [0, 1, 2, 3]

    led_command_class = None

    stop_flag = False

    mode = 0

    interval = 0

    # Colors
    (GREEN, YELLOW, RED) = ('green', 'yellow', 'red')
    colors = ['green', 'yellow', 'red']

    primary_color = 'green'

    # State
    green = False
    yellow = False
    red = False

    def __init__(self):
        # Init of Superclass
        threading.Thread.__init__(self)
        # Set Logger
        self.logger = BineeLogger(BineeLogger.LED, self.__class__.__name__)
        # Check if LEDs are available
        if not Device.has_led():
            self.logger.warning('Initialized LED-Control. But this '
                                'Binee Model (' + str(Device.bin_model) +
                                ') has no LEDs.')

        if Device.bin_model == 'presentation':
            self.led_command_class = PresentationLED()
        elif Device.bin_model == 'cardboard':
            self.led_command_class = CardboardLED()

        # Init Mode
        self.mode = LEDControl.OFF
        # Set primary color
        self.primary_color = LEDControl.GREEN
        # Init States
        green = False
        yellow = False
        red = False
        # Set Daemon
        self.daemon = True

    def run(self):
        self.logger.info('Start LED Control Thread.')
        # Reset Stop Flag
        self.stop_flag = False
        # Start Loop
        self._manage()

    def stop(self):
        self.logger.info('Stop LED Control Thread')
        self.stop_flag = True

    def set_mode(self, mode, color='green', interval=0):
        '''
        Set a mode available in LEDControl - RUN/BLINK/NONE
        Set the primary color
        Set the interval
        '''
        # Set all LEDs off
        self._off_mode()

        if mode in self.modes:
            self.mode = mode
        else:
            self.logger.error('Wrong LED Mode has been set. NONE will be used.')
            self.mode = LEDControl.OFF

        if color in self.colors:
            self.primary_color = color
        else:
            self.logger.error('Color is not valid. Will be set to Green.')
            self.primary_color = LEDControl.GREEN

        if interval < 1:
            self.logger.error('Invalid Interval has been set.')
            self.interval = 0
            return

        self.interval = interval

        self.logger.info('New LED Mode set. Props: Mode - ' + str(mode) +
                         ' Prim_Col - ' + str(color) + ' Interval - ' + str(interval))

    def _manage(self):

        while not self.stop_flag:
            # Interval between changes
            sleep(self.interval)
            # Check LED
            if self.led_command_class is None:
                self.logger.warning('No LED Command Class available.')
                continue

            # TODO: Set Functions into a variable directly?
            if self.mode == LEDControl.BLINK:
                self._blink_mode()
            elif self.mode == LEDControl.RUN:
                self._run_mode()
            elif self.mode == LEDControl.OFF:
                self._off_mode()
            elif self.mode == LEDControl.STATUS:
                self._status_mode()

    # ------------------------- Modes ----------------------------#

    def _blink_mode(self):

        self._set_status_of_color(not self._get_state_of_color(self.primary_color),
                                  self.primary_color)

    run_mode_index = 0

    def _run_mode(self):
        color_seq = {0: 'green', 1: 'yellow', 2: 'red', 3: 'green', 4: 'yellow', 5: 'red'}
        status_seq = {0: True, 1: True, 2: True, 3: False, 4: False, 5: False}

        self._set_status_of_color(status_seq[self.run_mode_index], color_seq[self.run_mode_index])

        self.run_mode_index = (self.run_mode_index + 1) % 6

    def _off_mode(self):
        self._set_led_status(green=False, yellow=False, red=False)

    def _status_mode(self):
        self._set_status_of_color(True, self.primary_color)

    # ------------------------- Helper ----------------------------#

    def _get_state_of_color(self, color):
        prim_color_status = None
        if color == LEDControl.GREEN:
            prim_color_status = self.green
        elif color == LEDControl.YELLOW:
            prim_color_status = self.yellow
        elif color == LEDControl.RED:
            prim_color_status = self.red
        return prim_color_status

    def _set_status_of_color(self, status, color):
        if color == LEDControl.GREEN:
            self._set_led_status(status, self.yellow, self.red)
        elif color == LEDControl.YELLOW:
            self._set_led_status(self.green, status, self.red)
        elif color == LEDControl.RED:
            self._set_led_status(self.green, self.yellow, status)

    def _set_led_status(self, green, yellow, red):
        assert isinstance(green, bool)
        assert isinstance(yellow, bool)
        assert isinstance(red, bool)

        self.green = green
        self.yellow = yellow
        self.red = red

        self.led_command_class.set_led(green, yellow, red)

    def set_cam_status(self, status):
        '''
        This Method is setting the LEDs of the Cam according to the status.
        on=1 -> LED is on, on=1 -> LED is off
        '''
        if status == "on":
            self.led_command_class.set_cam(on=1)
        if status == "off":
            self.led_command_class.set_cam(on=0)