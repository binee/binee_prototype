'''
This module is containing the NFC Control class
'''

# Imports
import hashlib
import threading
from time import sleep

from binee_logging import BineeLogger
from source.utils import MFRC522

# Module Statics
__author__ = 'David Moeller'
__version__ = "$Revision$"


class NFCControl(threading.Thread):

    reader = None

    interval = 0.1

    card_type = None
    card_id = None
    card_hashed_id = None
    card_found = False

    stop_flag = False

    def __init__(self):
        # Init of Superclass
        threading.Thread.__init__(self)
        # Set Logger
        self.logger = BineeLogger(BineeLogger.LED, self.__class__.__name__)
        # TODO: Check if Device has NFC

        # Setup reader
        self.reader = MFRC522.MFRC522()
        self.daemon = True

    def run(self):
        self.logger.info('Start NFC Control Thread.')
        # Reset Stop Flag
        self.stop_flag = False
        # Start Loop
        self._manage()

    def stop(self):
        self.logger.info("Stop NFC Control.")
        self.stop_flag = True

    def has_card(self):
        return self.card_found

    def _manage(self):

        while not self.stop_flag:
            # Interval between changes
            sleep(self.interval)
            # Check ID
            self._check_card_id()

    def _check_card_id(self):

        self.card_type, self.card_id = self._get_card_id()

        if self.card_id is not None:
            self.card_found = True
            self.card_hashed_id = self.hash_id(self.card_id)
        else:
            self.card_found = False
            self.card_hashed_id = None

    def _get_card_id(self):

        # Scan for cards
        (status, tag_type) = self.reader.MFRC522_Request(self.reader.PICC_REQIDL)

        # If a card is not found
        if status is not self.reader.MI_OK:
            return 'rfid', None

        # Get the UID of the card
        (status, uid) = self.reader.MFRC522_Anticoll()

        # If we have the UID, continue
        if status is not self.reader.MI_OK:
            return 'rfid', None

        card_id = str(uid[0]) + str(uid[1]) + str(uid[2]) + str(uid[3])
        # Print UID
        self.logger.info("Found Card with CardID: " + str(card_id))
        return 'rfid', card_id

    @staticmethod
    def hash_id(value):
        return hashlib.md5(value).hexdigest()
