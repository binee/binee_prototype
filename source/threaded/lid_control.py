'''
This module is containing the Lid class
'''

# Imports
import threading
from time import sleep

from binee_device import Device
from binee_logging import BineeLogger
from utils.cardboard.cardboard_lid import CardboardLid

from source.utils.presentation.presentation_lid import PresentationLid

# Module Statics
__author__ = 'David Moeller'
__version__ = "$Revision$"


class LidControl(threading.Thread):

    # Threading Propertie
    stop_flag = False

    # Interval for Event Loop
    interval = 0.5

    # Low Level Command Class
    lid_command_class = None

    # Event Loop Queue
    command_queue = None

    # Running
    running = None

    # Possible Commands
    (OPEN, CLOSE, HARDCLOSE) = ('open', 'close', 'hardclose')
    commands = ['open', 'close', 'hardclose']

    def __init__(self):
        # Init of Superclass
        threading.Thread.__init__(self)
        # Set Logger
        self.logger = BineeLogger(BineeLogger.MANAGER, self.__class__.__name__)
        # Check if LEDs are available
        if not Device.has_drum():
            self.logger.warning('Initialized Lid-Control. But this '
                                'Binee Model (' + str(Device.bin_model) +
                                ') has no Lid.')

        if Device.bin_model == 'presentation':
            self.lid_command_class = PresentationLid()
        elif Device.bin_model == 'cardboard':
            self.lid_command_class = CardboardLid()

        self.command_queue = []
        self.running = False
        self.daemon = True

    def is_running(self):
        return self.running

    # ------------------- Threading Methods ----------------------#

    def run(self):
        self.logger.info('Start Lid Control Thread.')
        # Reset Stop Flag
        self.stop_flag = False
        # Start Loop
        self._manage()

    def stop(self):
        self.stop_flag = True

    # ------------------- Drum Commands ----------------------#

    def set_command(self, command):
        '''
        Set Command for Drum
        '''

        if not isinstance(command, str):
            raise TypeError(self.logger.error('Command is not of Type String.'))

        if command not in self.commands:
            raise ValueError(self.logger.error('Command is not available.'))

        self.command_queue.append(command)
        self.logger.info('Set Lid Command: ' + str(command))

    def _manage(self):

        while not self.stop_flag:
            # Interval between changes
            sleep(self.interval)
            # Check LED
            if self.lid_command_class is None:
                self.logger.warning('No Lid Command Class available.')
                self.stop_flag = True
                continue

            # Check Event Queue
            if len(self.command_queue) == 0:
                self.running = False
                continue

            self.running = True

            # Get first command and remove it from queue
            command = self.command_queue.pop(0)

            # Process command
            self.process_command(command)

    def process_command(self, command):

        try:
            if command == LidControl.OPEN:
                self.lid_command_class.open_position()
            elif command == LidControl.CLOSE:
                self.lid_command_class.close_position()
            elif command == LidControl.HARDCLOSE:
                self.lid_command_class.hardclose_position()
        except:
            self.lid_command_class.reset()
