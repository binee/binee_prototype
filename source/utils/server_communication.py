'''
The Server Communication File is providing APIs to access the Online API
'''

# Imports
import base64
import hashlib
import os
import random
import time

import requests
from binee_logging import BineeLogger

from source.utils.singleton import Singleton

# Module Statics
__author__ = 'David Moeller'
__version__ = "$Revision$"


@Singleton
class Communicator:
    '''
    To get the instance call Communicator.Instance()
    '''

    host = None
    host_clean = None
    user = None
    pwd = None

    def __init__(self):
        # Initialise the Logger
        self.logger = BineeLogger(BineeLogger.MANAGER, self.__class__.__name__)
        # Set api adresses and User/PW
        self.host = "https://api.dev.binee.com"
        self.host_clean = "api.dev.binee.com"
        self.user = "moss"
        self.pwd = "binee"

    def check_connection(self):
        if os.system("ping -c 1 " + self.host_clean) == 0:
            return True
        else:
            return False

    def get_salt(self):
        # TODO: Safe to send Salt? Is this encrypted?
        url = self.host + "/api/users/" + self.user + "/salt.json"
        salt_request = requests.get(url, verify=False)
        salt = salt_request.json()["salt"]
        self.logger.debug('get_salt - Method, URL: ' + str(url) +
                          ' Request Text: ' + str(salt_request.text) +
                          ' Salt: ' + str(salt))
        return salt

    def get_token(self):
        salt = self.get_salt()
        url = self.host + "/api/users/tokens.json"
        payload = {'username': self.user, 'password': self.pwd, 'salt': salt}
        # TODO: Salt is sent again? Encrypted?
        token_request = requests.post(url, data=payload, verify=False)
        token = token_request.json()["token"]
        self.logger.debug('get_token - Method, URL: ' + str(url) +
                          ' Request Text: ' + str(token_request.text) +
                          ' Token: ' + str(token))
        return token

    def generate_nonce(self, length=16):
        # Generate pseudorandom number
        return base64.b64encode(''.join([str(random.randint(0, 9)) for _ in range(length)]))

    def create_wsse_header(self):
        nonce = self.generate_nonce()
        timestamp = time.strftime('%Y-%m-%dT%H:%M:%S', time.localtime()) + "+01:00"
        string = nonce + timestamp + self.get_token()
        digest = hashlib.sha1(string).hexdigest()
        xwsse = 'UsernameToken Username="' + self.user + '", PasswordDigest="' + base64.b64encode(
            digest) + '", Nonce="' + base64.b64encode(nonce) + '", Created="' + timestamp + '"'
        header = {"X-WSSE": xwsse}
        self.logger.debug('create_wsseHeader - Method' +
                          ' Digest: ' + str(digest) +
                          ' Header: ' + str(header))
        return header

    # ------------------------- General Request ----------------------------#

    def get_request(self, path):
        url = self.host + path
        get_request = requests.get(url, headers=self.create_wsse_header(), verify=False)
        self.logger.debug('get_request - Method' +
                          ' Request: ' + str(get_request))
        return get_request

    def post_request(self, path, data):
        url = self.host + path
        post_request = requests.post(url, headers=self.create_wsse_header(), data=data, verify=False)
        self.logger.debug('post_reqquest - Method' +
                          ' Request: ' + str(post_request))
        return post_request

    def put_request(self, path, data):
        url = self.host + path
        put_request = requests.put(url, headers=self.create_wsse_header(), data=data, verify=False)
        self.logger.debug('put_request - Method' +
                          ' Request: ' + str(put_request))
        return put_request

    # -------------------------- Image Methods -----------------------------#

    def upload_image(self, ticket, photopath):
        print "Uploading file..."
        url = self.host + "/images.json"
        # files = dict (imageFile = open('test_pic/'+str(i)+'.jpg'), ticket = ticket)
        # print header
        image_request = requests.post(url, headers=self.create_wsse_header(), data={'ticket': ticket},
                          files=[('imageFile', ('imageFile', open(photopath, 'rb'), 'image/jpeg'))], verify=False)
        print image_request.text
        return image_request
