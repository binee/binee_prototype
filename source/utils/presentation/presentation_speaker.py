'''
This module is containing the Cardboard LED Commands
'''

# Imports
from binee_logging import BineeLogger
import os

# Module Statics
__author__ = 'David Moeller'
__version__ = "$Revision$"


class PresentationSpeaker:

    loudness = None

    def __init__(self):
        self.logger = BineeLogger(BineeLogger.MANAGER, self.__class__.__name__)
        # Init Loudness
        self.loudness = '200'

    def say(self, text):
        command = "espeak -a " + self.loudness + " '" + text + "'"
        os.system(command)
