'''
This module is containing the Cardboard LED Commands
'''

# Imports
import RPi.GPIO as GPIO
from binee_logging import BineeLogger

from source.utils import LedBaseClass

# Module Statics
__author__ = 'David Moeller'
__version__ = "$Revision$"


class PresentationLED(LedBaseClass):

    def __init__(self):
        self.logger = BineeLogger(BineeLogger.LED, self.__class__.__name__)
        # Setup
        self._setup_pins()

    def _setup_pins(self):
        # Setup Boardlayout
        GPIO.setmode(GPIO.BOARD)

        # Setup LED Pins
        self.red_pin = 12
        self.yellow_pin = 10
        self.green_pin = 8

        self.camera_pin = 18

        # Set pins to output
        GPIO.setup(self.red_pin, GPIO.OUT)  # red
        GPIO.setup(self.yellow_pin, GPIO.OUT)  # yellow
        GPIO.setup(self.green_pin, GPIO.OUT)  # green
        GPIO.setup(self.camera_pin, GPIO.OUT)  # camera
