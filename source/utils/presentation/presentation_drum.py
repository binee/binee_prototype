'''
This module is containing the Presentation Drum Commands
'''

# Imports
from binee_logging import BineeLogger
from time import sleep
import RPi.GPIO as GPIO
import pigpio
import os

# Module Statics
__author__ = 'David Moeller'
__version__ = "$Revision$"


class PresentationDrum:

    def __init__(self):
        self.logger = BineeLogger(BineeLogger.MANAGER, self.__class__.__name__)

        # print "Start pigpio daemon"
        os.system("sudo pigpiod")  # run pigpio on pcm
        # Set Boardlayout
        GPIO.setmode(GPIO.BOARD)

        # Set Stepper Pins
        self.step_pins = [31, 33, 37, 35]

        for pin in self.step_pins:
            GPIO.setup(pin, GPIO.OUT)
            GPIO.output(pin, False)

        self.stepper_speed = 0.001

    def cam_position(self):
        self.rotate(1600)

    def input_position(self):
        self.rotate(2700)

    def output_position(self):
        self.rotate(1000)

    def dump_position(self):
        self.rotate(1000)
        self.rotate(2700)

    def reset(self):
        self.logger.info('No Reset implemented.')

    def rotate(self, angle):
        seq_2 = range(0, 8)
        seq_2[0] = [1, 0, 0, 0]
        seq_2[1] = [1, 0, 0, 1]
        seq_2[2] = [0, 0, 0, 1]
        seq_2[3] = [0, 0, 1, 1]
        seq_2[4] = [0, 0, 1, 0]
        seq_2[5] = [0, 1, 1, 0]
        seq_2[6] = [0, 1, 0, 0]
        seq_2[7] = [1, 1, 0, 0]

        steps = angle  # inputposition
        print steps
        seq = seq_2
        step_count = 8

        i = 1
        step_counter = 0

        while i <= steps:

            for pin in range(0, 4):
                xpin = self.step_pins[pin]
                if seq[step_counter][pin] != 0:
                    print " Step %i Enable %i" % (step_counter, xpin)
                    GPIO.output(xpin, True)
                else:
                    GPIO.output(xpin, False)

            step_counter += 1

            # If we reach the end of the sequence
            # start again
            if step_counter == step_count:
                step_counter = 0
            if step_counter < 0:
                step_counter = step_count

                # Wait before moving on (moving speed)
            sleep(self.stepper_speed)

            i += 1