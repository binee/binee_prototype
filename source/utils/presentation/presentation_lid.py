'''
This module is containing the Presentation Lid Commands
'''

# Imports
from binee_logging import BineeLogger
from time import sleep
import RPi.GPIO as GPIO
import pigpio
import os

# Module Statics
__author__ = 'David Moeller'
__version__ = "$Revision$"


class PresentationLid:

    def __init__(self):
        self.logger = BineeLogger(BineeLogger.MANAGER, self.__class__.__name__)

        # print "Start pigpio daemon"
        os.system("sudo pigpiod")  # run pigpio on pcm
        # Set Boardlayout
        GPIO.setmode(GPIO.BOARD)

        # Set Servo Settings for lid, according pigpio
        self.servo_pin = 12  # GPIO layout!!!!!! BOARD32=GPIO12
        self.pi1 = pigpio.pi()

    def hardclose_position(self):
        self.pi1.set_servo_pulsewidth(self.servo_pin, 1900)
        sleep(0.5)

    def close_position(self):
        for i in range(500, 1900, 10):
            self.pi1.set_servo_pulsewidth(self.servo_pin, i)
            sleep(0.003)

    def open_position(self):
        for i in range(1900, 500, (-10)):
            self.pi1.set_servo_pulsewidth(self.servo_pin, i)
            sleep(0.003)
            self.pi1.set_servo_pulsewidth(self.servo_pin, 0)

    def reset(self):
        self.logger.info('No Reset implemented.')