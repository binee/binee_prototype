'''
This module is containing the Cardboard Drum Commands
'''

# Imports
import RPi.GPIO as GPIO
from binee_logging import BineeLogger

from cardboard_motor import CardboardMotor

# Module Statics
__author__ = 'David Moeller'
__version__ = "$Revision$"


class CardboardDrum(CardboardMotor):

    def __init__(self):
        self.logger = BineeLogger(BineeLogger.MANAGER, self.__class__.__name__)
        # Super Class Init
        CardboardMotor.__init__(self)

    def cam_position(self):
        self.motor_start(self.motor_drum, "right")
        while GPIO.input(self.drum_cam) != 1 and GPIO.input(self.drum_out) != 1:
            print "Moving to camera position"
        self.motor_stop(self.motor_drum)

    def input_position(self):
        self.motor_start(self.motor_drum, "left")
        while GPIO.input(self.drum_in) != 1:
            print "Moving drum to input position..."
        self.motor_stop(self.motor_drum)

    def output_position(self):
        self.motor_start(self.motor_drum, "right")
        while GPIO.input(self.drum_out) != 1:
            print "Moving drum to dump position..."
        self.motor_stop(self.motor_drum)

    def dump_position(self):
        self.motor_start(self.motor_drum, "right")
        while GPIO.input(self.drum_out) != 1:
            print "Moving drum to dump position..."
        self.motor_stop(self.motor_drum)
        self.motor_start(self.motor_drum, "left")
        while GPIO.input(self.drum_in) != 1:
            print "Moving drum to input position..."
        self.motor_stop(self.motor_drum)