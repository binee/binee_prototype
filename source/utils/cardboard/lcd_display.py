'''
This module is containing the Display Specific classes
'''

# Imports
from binee_logging import BineeLogger
import json

# Module Statics
__author__ = 'David Moeller'
__version__ = "$Revision$"


class LcdDisplay:

    def __init__(self):
        # Init Logger
        self.logger = BineeLogger(BineeLogger.MANAGER, self.__class__.__name__)

    def write_display_properties(self, trigger=1, backlight='on'):
        return {'trigger': str(trigger), 'backlight': backlight}

    def write_line_one(self, text, pos=2):
        return self.write_line(text, pos, 1)

    def write_line_two(self, text, pos=7):
        return self.write_line(text, pos, 2)

    def write_line(self, text, pos, line_number):
        return {'line' + str(line_number): {'text': text, 'pos': pos}}

    def dump_display_file(self, display_properties, *args):
        display_data = display_properties

        # Add written lines
        for arg in args:
            display_data.update(arg)

        # Dump Files
        with open('quick2wire-python-api/lcd.json', 'w') as display_file:
            json.dump(display_data, display_file)
            display_file.close
