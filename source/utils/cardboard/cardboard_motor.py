'''
This module is containing the Cardboard Drum Commands
'''

# Imports
import RPi.GPIO as GPIO

# Module Statics
__author__ = 'David Moeller'
__version__ = "$Revision$"


class CardboardMotor:

    def __init__(self):
        # Set Boardlayout
        GPIO.setmode(GPIO.BOARD)

        # Set up Motors
        # 1A,1B set direction motor 1
        # E1 PWM signal for speed
        self.one_a = 33
        self.one_b = 35
        self.e_one = 37

        self.two_a = 36
        self.two_b = 38
        self.e_two = 40

        chan_list = [self.one_a, self.one_b, self.e_one, self.two_a, self.two_b, self.e_two]
        GPIO.setup(chan_list, GPIO.OUT)
        GPIO.output(chan_list, 0)

        # Set motor Channels
        self.motor_drum = 2
        self.motor_lid = 1

        # Set Pins for EndSwitches
        self.lid_close = 16
        self.lid_open = 18

        self.drum_in = 29
        self.drum_cam = 31
        self.drum_out = 7

        chan_list = [self.lid_close, self.lid_open, self.drum_in, self.drum_cam, self.drum_out]
        GPIO.setup(chan_list, GPIO.IN)

    def motor_stop(self, motor):
        # TODO: Motor Value/Type safe?
        if motor == 1:
            GPIO.output(self.one_a, 0)
            GPIO.output(self.one_b, 0)
            GPIO.output(self.e_one, 0)
        if motor == 2:
            GPIO.output(self.two_a, 0)
            GPIO.output(self.two_b, 0)
            GPIO.output(self.e_two, 0)

    def motor_start(self, motor, direction):
        # TODO: Motor/Direction Value/Type safe?
        if motor == 1:
            pin_a = self.one_a
            pin_b = self.one_b
            pin_e = self.e_one

        if motor == 2:
            pin_a = self.two_a
            pin_b = self.two_b
            pin_e = self.e_two

        # TODO: Pin Missing?
        if direction == "left":
            GPIO.output(pin_b, 1)
            GPIO.output(pin_e, 1)

        if direction == "right":
            GPIO.output(pin_a, 1)
            GPIO.output(pin_b, 0)
            GPIO.output(pin_e, 1)

    def reset(self):
        GPIO.output(self.one_a, 0)
        GPIO.output(self.one_b, 0)
        GPIO.output(self.e_one, 0)
        GPIO.output(self.two_a, 0)
        GPIO.output(self.two_b, 0)
        GPIO.output(self.e_two, 0)
