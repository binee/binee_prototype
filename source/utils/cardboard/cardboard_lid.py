'''
This module is containing the Cardboard Lid Commands
'''

# Imports
import RPi.GPIO as GPIO
from binee_logging import BineeLogger

from cardboard_motor import CardboardMotor

# Module Statics
__author__ = 'David Moeller'
__version__ = "$Revision$"


class CardboardLid(CardboardMotor):

    def __init__(self):
        self.logger = BineeLogger(BineeLogger.MANAGER, self.__class__.__name__)
        # Super Class Init
        CardboardMotor.__init__(self)

    def open_position(self):
        self.motor_start(self.motor_lid, "left")
        while GPIO.input(self.lid_open) != 1:
            pass
            # print "Lid opening"
        print ("Lid opened.")
        self.motor_stop(self.motor_lid)

    def close_position(self):
        self.motor_start(self.motor_lid, "right")
        while GPIO.input(self.lid_close) != 1:
            print "Lid closing..."
        print "Lid closed."
        self.motor_stop(self.motor_lid)

    def hardclose_position(self):
        self.motor_start(self.motor_lid, "right")
        while GPIO.input(self.lid_close) != 1:
            print "Lid closing..."
        print "lid closed."
        self.motor_stop(self.motor_lid)