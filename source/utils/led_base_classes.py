'''
This module is containing the Cardboard LED Commands
'''

# Imports
import RPi.GPIO as GPIO

# Module Statics
__author__ = 'David Moeller'
__version__ = "$Revision$"


class LedBaseClass(object):
    # TODO:Class Name According to Pi Module Name

    red_pin = None
    yellow_pin = None
    green_pin = None
    camera_pin = None

    logger = None

    def set_led(self, green=False, yellow=False, red=False):
        '''
        '''
        mapper = {False: GPIO.LOW, True: GPIO.HIGH}

        GPIO.output(self.green_pin, mapper[green])
        GPIO.output(self.yellow_pin, mapper[yellow])
        GPIO.output(self.red_pin, mapper[red])

    def set_cam(self, on):
        mapper = {False:GPIO.LOW, True:GPIO.HIGH}

        GPIO.output(self.camera_pin, mapper[on])
