'''
Main File which is handling all states of the binee bin prototype.
'''

# Imports
import time
import unittest

# Binee Imports
from threaded.led_control import LEDControl
from threaded.drum_control import DrumControl
from threaded.lid_control import LidControl

# Module Statics
__author__ = 'David Moeller'
__version__ = "$Revision$"


class BineeTestCase(unittest.TestCase):

    def suite(self):
        return unittest.TestLoader().loadTestsFromTestCase(self.__class__)

    def runTest(self):
        pass

if __name__ == '__main__':
    # Check LEDs
    led_control = LEDControl()
    led_control.daemon = True
    led_control.start()
    led_control.set_mode(LEDControl.RUN)
    time.sleep(5)
    led_control.set_mode(LEDControl.BLINK)
    time.sleep(5)
    led_control.stop()

    # Check Drum
    drum_control = DrumControl()
    drum_control.daemon = True
    drum_control.start()
    drum_control.set_command(DrumControl.INPUT)
    while drum_control.is_running():
        time.sleep(1)
    drum_control.set_command(DrumControl.OUTPUT)
    while drum_control.is_running():
        time.sleep(1)
    drum_control.set_command(DrumControl.CAM)
    while drum_control.is_running():
        time.sleep(1)
    drum_control.set_command(DrumControl.DUMP)
    while drum_control.is_running():
        time.sleep(1)
    drum_control.stop()

    # Check Lid
    lid_control = LidControl()
    lid_control.daemon = True
    lid_control.start()
    lid_control.set_command(LidControl.OPEN)
    while lid_control.is_running():
        time.sleep(1)
    lid_control.set_command(LidControl.HARDCLOSE)
    while lid_control.is_running():
        time.sleep(1)
    lid_control.set_command(LidControl.OPEN)
    while lid_control.is_running():
        time.sleep(1)
    lid_control.set_command(LidControl.CLOSE)
    while lid_control.is_running():
        time.sleep(1)
    lid_control.stop()
