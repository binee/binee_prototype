#This code is supposed to run on a Raspberry Pi  at the binee prototypes.
#
#Working on prototype versions:
#
#Cardboard
#Presentation
#
#Florian Eidner



#Set the prototype:

#Set binee parameters:
good_type = "cellphone"
bin_name = "bernd"

#Import Modules:
import hashlib
import logging
import threading
import time

import picamera
import requests

import users_file as db
from source.utils import MFRC522

#### READ CONFIG FILE
with open('config.binee') as f:
	conf = f.readlines()
conf_lines = len(conf)
for i in range(0,conf_lines):
	conf[i] = conf[i].replace('\n','')
bin_model= conf[0]

if bin_model == "cardboard":
	print "Import bin modules"
	import cardboard_motor as motor
	import cardboard_lcd as lcd
	import cardboard_led as led

elif bin_model == "presentation":
	print "Import bin modules"
	import presentation_motor as motor
	import presentation_led as led
	import presentation_speaker as speaker

import server_communication as api

speaker.say("Im ready")
#Setup Camera
cam = picamera.PiCamera()
cam.resolution = (1000, 750) #res.max= 2592x1944
cam.brightness = 57

MIFAREReader = MFRC522.MFRC522()

bin_states = {
        "locked" : "null",
        "0" : "booting",
        "1" : "startPage",
        "1.1": "thankyou",
        "2.1" : "helloRegisteredUser",
        "2.2" : "helloForeigner",
        "2.3" : "firstContact",
        "3.0" : "processing",
        "3.1" : "thankYou",
        "3.2" : "confirmKey",
        "3.3" : "confirmPairing",
        "4.1" : "userRegistered",
        "4.2" : "error",
        "4.3" : "missingResource",
        "5" : "stopped"
    };



def get_MAC(interface):     #Get devices MAC-Adress, Interface= wlan0, eth0
                            #Return MAC as String   
    try:
      str = open('/sys/class/net/%s/address' %interface).readline()
    except:
      str = "00:00:00:00:00:00"
    return str[0:17]

def create_binID(MAC):      #Create BinID from MAC-Adress
    ID= MAC[0:2]+MAC[3:5]+MAC[6:8]+MAC[9:11]+MAC[12:14]+MAC[15:17]
    return ID

def get_location():
	r = requests.get('http://freegeoip.net/json')
	j = json.loads(r.text)
	lat = j['latitude']
	lon = j['longitude']
	location =str(lat)+";"+str(lon)
	return location

def photo(filename):
    print "Taking photo ..."
    led.cam("on") 
    cam.capture(filename)
    led.cam("off")

def get_cardID():
           
        cardID = 0

        # Scan for cards
        (status,TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)

        # If a card is found
        if status == MIFAREReader.MI_OK:
            print "Card detected"
    
        # Get the UID of the card
        (status,uid) = MIFAREReader.MFRC522_Anticoll()

        # If we have the UID, continue
        if status == MIFAREReader.MI_OK:

            cardID = str(uid[0])+str(uid[1])+str(uid[2])+str(uid[3])
            # Print UID
            print cardID
        
        if cardID == 0:
            print "No RFID found"
        
        return ('rfid',cardID)

def hash_ID(value):
    
    hashedID = hashlib.md5(value).hexdigest()
    return hashedID

def set_binState(status):
	
	status_text = status + bin_states[status]
	api.post("/api/bins/states",data={'state':status_text})

	if status == "1":
		if bin_model == "cardboard":
			lcd.write(0,"Hey, I am binee!",0,"Identify via NFC.")
		led.status("green")

	if status == "2.2":
		if bin_model == "cardboard":
			lcd.write(0,"Hey gorgeous!",0,"Feed me.")
		led.status("green")



def __init__():

	set_binState("0")

	print "Starting Up..."
	if bin_model == "cardboard":
		lcd.write(2,"binee starting",7,"...")
	leds=led.run()
	t = threading.Thread(target=leds.go, args=(10,))
	t.start()


	print "Creating BinID..."
	if bin_model == "cardboard":
		lcd.write(1,"binee starting",1,"inner values...")
	global binID

	MAC = get_MAC("wlan0")
	binID = create_binID(MAC)
	print binID



	#Set motor positions for beginning
	if bin_model == "cardboard":
		lcd.write(1,"binee starting",3,"motors...")
	drum=motor.drum()
	lid=motor.lid()
	drum.move("input")
	lid.move("hardclose")
	if bin_model == "cardboard":
		lcd.write(1,"binee starting",5,"nfc...")
	



	if bin_model == "cardboard":
		lcd.write(2,"binee starting",3,"server...")
	server_status = api.check_connection()
	if server_status == 1:
		"Server connection established"
	else:
		"Server not reachable, switch to offline mode"

	############################
	# Initialization sucessful #
	############################
	leds.terminate()
	global bin_state
	bin_state="1"
	set_binState(bin_state)

def __main__():
		global bin_state

		cardID = [0,0]
		now=time.time()
		while cardID[1] == 0:
			if time.time()> (now+5) and bin_state != "1":
				lcd.write(0,"Hey, I am binee!",0,"Identify via NFC.")
				bin_state = "1"
			cardID = get_cardID()
			print "wait for card"
		
		lid = motor.lid()
		lid_rotation = threading.Thread(target=lid.move, args=("open",))
		lid_rotation.start()

		hashedID= hash_ID(cardID[1])
		
		logging.debug("Get User ID from File.")
		new_id = db.id_last_line(db.last_line("test_users.txt"))+1
		
		id_str = "Your ID: "+str(new_id)
		if bin_model == "cardboard":
			lcd.write(5,"Hello!",3,id_str)

		logging.debug("Write to file.")
		line = str(new_id)+" "+cardID[1]
		db.add_line("test_users.txt",line)
		
		logging.debug("look if card is already paired.")
		card_status = api.post("/api/searches/pairings.json",data={'token':hashedID, 'type':cardID[0]})
		now=time.time()
		card_status = 0
		if card_status == 1:
			set_binState("2.1")
			while time.time() < (now+10):
				if get_cardID()[1] == cardID[1]:
					break

		if card_status == 0:
			set_binState("2.2")
			while time.time() < (now+10):
				if get_cardID()[1] == cardID[1]:
					break
		if bin_model == "cardboard":
			lcd.write(3,"Thank you!",3,id_str)
		lid.move("close")
		
		set_binState("3.0")
		drum= motor.drum()
		drum.move("cam")
		
		photopath= "pictures/"+time.strftime('%Y-%m-%dT%H:%M:%S', time.localtime())+"+01:00.jpg"
		photo(photopath)
		
		drum = motor.drum()
		drum_rotation = threading.Thread(target=drum.move, args=("dump",))
		drum_rotation.start()

		#ticket=api.post("/api/tickets.json",data={'type':'rfid','token':hashedID,'good':good_type, 'quantity':'1'})
		#ticketID=ticket.json()['tickID']
		#api.upload(ticketID,photopath)
		bin_state="1.1"
		set_binState(bin_state)
		

__init__()

while True:
	__main__()
