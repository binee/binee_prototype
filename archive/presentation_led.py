import RPi.GPIO as GPIO
from time import sleep

#Setup Boardlayout
GPIO.setmode(GPIO.BOARD)

#Setup LED Pins
redpin= 12
yellowpin= 10
greenpin= 8

camerapin= 18

#Set pins to output
GPIO.setup(redpin,GPIO.OUT) #red
GPIO.setup(yellowpin,GPIO.OUT) #yellow
GPIO.setup(greenpin,GPIO.OUT) #green
GPIO.setup(camerapin,GPIO.OUT) #camera

def status(color):
    if status=="green":
        GPIO.output(greenpin,GPIO.HIGH)
        GPIO.output(yellowpin,GPIO.LOW)
        GPIO.output(redpin,GPIO.LOW)

    if status=="yellow":
        GPIO.output(greenpin,GPIO.LOW)
        GPIO.output(yellowpin,GPIO.HIGH)
        GPIO.output(redpin,GPIO.LOW)

    if status=="red":
        GPIO.output(greenpin,GPIO.LOW)
        GPIO.output(yellowpin,GPIO.LOW)
        GPIO.output(redpin,GPIO.HIGH)
    
    if status=="all":
        GPIO.output(greenpin,GPIO.HIGH)
        GPIO.output(yellowpin,GPIO.HIGH)
        GPIO.output(redpin,GPIO.HIGH)
    
    if status=="off":
        GPIO.output(greenpin,GPIO.LOW)
        GPIO.output(yellowpin,GPIO.LOW)
        GPIO.output(redpin,GPIO.LOW)

class blink:
	def __init__(self):
		self._running = True

	def go(self,color,blinkspeed):
				if color == "green":
					pin = greenpin
				if color == "red":
					pin = redpin
				if color == "yellow":
					pin = yellowpin

				GPIO.output(pin,GPIO.HIGH) 
				sleep(1/blinkspeed)
				GPIO.output(pin,GPIO.LOW)
				sleep(1/blinkspeed)
	def terminate(self):
		self._running = False


class run:

	def __init__(self):
		self._running = True

	def go(self,runspeed):
		GPIO.output(greenpin,GPIO.HIGH) 
		sleep(1/runspeed)
		GPIO.output(yellowpin,GPIO.HIGH) 
		sleep(1/runspeed)
		GPIO.output(redpin,GPIO.HIGH) 
		sleep(1/runspeed)
		GPIO.output(greenpin,GPIO.LOW) 
		sleep(1/runspeed)
		GPIO.output(yellowpin,GPIO.LOW) 
		sleep(1/runspeed)
		GPIO.output(redpin,GPIO.LOW) 
		sleep(1/runspeed)

	def terminate(self):
		self._running = False

def cam(status):
	if status=="on":
		GPIO.output(camerapin,GPIO.HIGH)

	if status=="off":
		GPIO.output(camerapin,GPIO.LOW)