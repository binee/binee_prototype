#This is the module for the motors of the cardboard binee.

#import more modules
import time
import RPi.GPIO as GPIO

#Set Boardlayout
GPIO.setmode(GPIO.BOARD)


#Set up Motors
# 1A,1B set direction motor 1
# E1 PWM signal for speed
oneA = 33
oneB = 35
Eone = 37

twoA = 36
twoB = 38
Etwo = 40

chan_list = [oneA,oneB,Eone,twoA,twoB,Etwo]
GPIO.setup(chan_list,GPIO.OUT)
GPIO.output(chan_list,0)

#Set motor Channels
motor_drum = 2
motor_lid = 1


#Set Pins for EndSwitches
lid_close = 16
lid_open = 18

drum_in = 29
drum_cam = 31
drum_out = 7

chan_list = [lid_close,lid_open,drum_in,drum_cam,drum_out]
GPIO.setup(chan_list,GPIO.IN)


def motor_stop(motor):
    if motor == 1:
        GPIO.output(oneA,0)
        GPIO.output(oneB,0)
        GPIO.output(Eone,0)
    if motor == 2:
        GPIO.output(twoA,0)
        GPIO.output(twoB,0)
        GPIO.output(Etwo,0)

def motor_start(motor,direction):
    if motor == 1:
        pinA = oneA
        pinB = oneB
        pinE = Eone              
        
    if motor == 2:
        pinA = twoA
        pinB = twoB
        pinE = Etwo

    if direction == "left":
        GPIO.output(pinB,1)
        GPIO.output(pinE,1)

    if direction == "right":
        GPIO.output(pinA,1)
        GPIO.output(pinB,0)
        GPIO.output(pinE,1)

class lid:

	def __init__(self):
		self._running = True

	def move(self,position): 
		try:       
		    if position == "hardclose":
		        motor_start(motor_lid,"right")
		        while GPIO.input(lid_close) != 1:
		            print "Lid closing..."
		        print "lid closed."
		        motor_stop(motor_lid)      
		        
		    if position == "close":
		        motor_start(motor_lid,"right")
		        while GPIO.input(lid_close) != 1:
		            print "Lid closing..."
		        print "Lid closed."
		        motor_stop(motor_lid)
		            
		    if position == "open":
		        motor_start(motor_lid,"left")
		        while GPIO.input(lid_open) != 1:
		            pass
		            #print "Lid opening"
		        print ("Lid opened.")
		        motor_stop(motor_lid)
		except:
			reset()

	def terminate(self):
		self._running = False
		reset()

class drum:
    
	def __init__(self):
		self._running = True

	def move(self,position):
      
			if position == "cam":
				motor_start(motor_drum,"right")
				while GPIO.input(drum_cam) != 1 and GPIO.input(drum_out) != 1:
					print "Moving to camera position"
				motor_stop(motor_drum)      
			if position == "input":
				motor_start(motor_drum,"left")
				while GPIO.input(drum_in) != 1:
					print "Moving drum to input position..."
				motor_stop(motor_drum)
			if position == "output":
				motor_start(motor_drum,"right")
				while GPIO.input(drum_out) != 1:
					print "Moving drum to dump position..."
				motor_stop(motor_drum)

			if position == "dump":
				motor_start(motor_drum,"right")
				while GPIO.input(drum_out) != 1:
					print "Moving drum to dump position..."
				motor_stop(motor_drum)
				motor_start(motor_drum,"left")
				while GPIO.input(drum_in) != 1:
					print "Moving drum to input position..."
				motor_stop(motor_drum)
			reset()
	
	def terminate(self):
		self._running = False
		reset()

def reset():
        GPIO.output(oneA,0)
        GPIO.output(oneB,0)
        GPIO.output(Eone,0)
        GPIO.output(twoA,0)
        GPIO.output(twoB,0)
        GPIO.output(Etwo,0)