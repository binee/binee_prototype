import requests
import os
import time
import socket
import hashlib
import base64
import logging
import sys
import random



host = "https://api.dev.binee.com"
host_clean="api.dev.binee.com"
user = "moss"
pswd = "binee"

def check_connection():
    if os.system("ping -c 1 "+host_clean) == 0:
        return 1
    else:
        return 0

def get_salt():
    url = host + "/api/users/"+user+"/salt.json"
    logging.debug(url)
    r = requests.get(url,verify=False)
    logging.debug(r.text)
    salt = r.json()["salt"]
    logging.debug(salt)
    return salt

def get_token():
    salt = get_salt()
    url = host + "/api/users/tokens.json"
    logging.debug(url)
    payload = {'username' : user,'password': pswd, 'salt':salt}
    r = requests.post(url,data=payload, verify=False)
    logging.debug(r.text)
    token = r.json()["token"]
    logging.debug(token)
    return token

def generate_nonce(length=16):
    #Generate pseudorandom number
    return base64.b64encode(''.join([str(random.randint(0, 9)) for i in range(length)]))

def create_wsseHeader():
    nonce = generate_nonce()
    timestamp = time.strftime('%Y-%m-%dT%H:%M:%S', time.localtime())+"+01:00"
    string = nonce+timestamp+get_token()
    digest= hashlib.sha1(string).hexdigest()
    logging.debug(digest)
    xwsse= 'UsernameToken Username="'+user+'", PasswordDigest="'+base64.b64encode(digest)+'", Nonce="'+base64.b64encode(nonce)+'", Created="'+timestamp+'"'
    header = {"X-WSSE":xwsse}
    logging.debug(header)
    return header

def get(path):
    url = host+path
    r=requests.get(url, headers=create_wsseHeader(),verify=False)
    logging.debug(r)
    return r

def post(path,data):
    url = host+path
    r=requests.post(url, headers=create_wsseHeader(),data=data,verify=False)
    logging.debug(r)
    return r

def put(path,data):
    url = host+path
    r=requests.put(url, headers=create_wsseHeader(),data=data,verify=False)
    logging.debug(r)
    return r

def upload(ticket,photopath):
	print "Uploading file..."
	url = host + "/images.json"
	#files = dict (imageFile = open('test_pic/'+str(i)+'.jpg'), ticket = ticket)
	#print header
	r = requests.post(url, headers=create_wsseHeader(), data= {'ticket': ticket }, files = [('imageFile', ('imageFile', open(photopath, 'rb'),'image/jpeg'))], verify=False)
	print r.text
	return r