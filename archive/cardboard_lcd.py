import os
import sys
import json

def write(line1_pos,line1_text,line2_pos,line2_text):
    lcd_data = {"trigger" : '1', "backlight" : 'on', 'line1' : {'text': line1_text, 'pos': line1_pos}, 'line2': {'text':line2_text, 'pos': line2_pos}}

    with open('quick2wire-python-api/lcd.json', 'w') as file:
            json.dump(lcd_data,file)
            file.close