#This is the module for the motors of the cardboard binee.

#import more modules
from time import sleep
import RPi.GPIO as GPIO
import pigpio
import os

#print "Start pigpio daemon"
os.system("sudo pigpiod") #run pigpio on pcm
#Set Boardlayout
GPIO.setmode(GPIO.BOARD)

#Set Stepper Pins
StepPins = [31,33,37,35]

for pin in StepPins:
    
    print "Setup pins"
    GPIO.setup(pin,GPIO.OUT)
    GPIO.output(pin, False)

stepper_speed= 0.001

#Set Servo Settings for lid, according pigpio
servo_pin = 12  #GPIO layout!!!!!! BOARD32=GPIO12
pi1 = pigpio.pi()

def rotate(angle):
    Seq2 = []
    Seq2 = range(0, 8)
    Seq2[0] = [1,0,0,0]
    Seq2[1] = [1,0,0,1]
    Seq2[2] = [0,0,0,1]
    Seq2[3] = [0,0,1,1]
    Seq2[4] = [0,0,1,0]
    Seq2[5] = [0,1,1,0]
    Seq2[6] = [0,1,0,0]
    Seq2[7] = [1,1,0,0]
    
    
    steps = angle #inputposition
    print steps
    Seq = Seq2
    StepCount = 8

    i=1
    StepCounter = 0
    
    while i <= steps:

        for pin in range(0, 4):
            xpin = StepPins[pin]
            if Seq[StepCounter][pin]!=0:
                print " Step %i Enable %i" %(StepCounter,xpin)
                GPIO.output(xpin, True)
            else:
                GPIO.output(xpin, False)

        StepCounter += 1

      # If we reach the end of the sequence
      # start again
        if (StepCounter==StepCount):
            StepCounter = 0
        if (StepCounter<0):
            StepCounter = StepCount

      # Wait before moving on (moving speed)
        sleep(stepper_speed)
      
        i +=1

class lid:

	def __init__(self):
		self._running = True

	def move(self,position):
		 # pigpio pulsing over pcm from 500-2500       
		if position == "hardclose":
		    pi1.set_servo_pulsewidth(servo_pin,1900)
        	sleep(0.5)
		if position == "close":
		    for i in range(500,1900,10):
                 	pi1.set_servo_pulsewidth(servo_pin,i)
            	sleep(0.003)
		            
		if position == "open":
		    for i in range(1900,500,(-10)):
                       	pi1.set_servo_pulsewidth(servo_pin,i)
            	sleep(0.003)
                pi1.set_servo_pulsewidth(servo_pin,0)

	def terminate(self):
		self._running = False

class drum:
    
	def __init__(self):
		self._running = True

	def move(self,position):
      
			if position == "cam":
				rotate(1600)

			if position == "input":
				rotate(2700)

			if position == "output":
				rotate(1000)

			if position == "dump":
				rotate(1000)
				rotate(2700)

	def terminate(self):
		self._running = False

